---
title: "Sobre"
date: 2022-10-06T17:10:13-03:00
draft: false
layout: about
---

Autor: Eric Toshio Mizukami - 11804272

Curso: Engenharia Mecatrônica

Este site foi feito com o gerador de sites estáticos Hugo. Trata-se de uma atividade da disciplina PMR3304 - Sistemas de Informação, da Escola Politécnica da Universidade de São Paulo.




