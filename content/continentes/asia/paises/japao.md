---
title: "Japão"
date: 2022-09-22T21:07:01-03:00
draft: false
capital: "Tóquio"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/435px-Flag_of_Japan.svg.png
pop: "125,8 milhões habitantes"
area: "377 975 km²"
dens: "332,83 habitantes/km²"
lingua: "Japonês"
moeda: "Iene"
---
