---
title: "Eritreia"
date: 2022-10-09T10:55:44-03:00
draft: false
capital: "Asmara"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Flag_of_Eritrea.svg/383px-Flag_of_Eritrea.svg.png
pop: "6,081 milhões habitantes"
area: "117 600 km²"
dens: "51,71 habitantes/km²"
lingua: "Tigrínia"
moeda: "Nakfa"
---

