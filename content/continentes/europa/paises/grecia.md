---
title: "Grécia"
date: 2022-10-09T10:57:56-03:00
draft: false
capital: "Atenas"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Greece.svg/383px-Flag_of_Greece.svg.png
pop: "10,72 milhões habitantes"
area: "131 957 km²"
dens: "81,24 habitantes/km²"
lingua: "Grego"
moeda: "Euro"
---

