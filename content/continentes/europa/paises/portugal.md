---
title: "Portugal"
date: 2022-10-09T10:57:50-03:00
draft: false
capital: "Lisboa"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/390px-Flag_of_Portugal.svg.png
pop: "10,31 milhões habitantes"
area: "92 212 km²"
dens: "111,81 habitantes/km²"
lingua: "Português"
moeda: "Euro"
---

