---
title: "Home"
date: 2022-09-22T20:53:38-03:00
draft: false
---

"Que mundo pequeno" eles dizem... Nada disso! O mundo em que vivemos é assustadoramente grande e nem os mais ambiciosos conseguem conhecê-lo por completo.

Aqui você irá encontrar informações como a capital dos países, a sua bandeira, população, área, densidade demográfica, moeda e língua oficial!

Comece escolhendo o continente e em seguida selecione o país que deseja explorar. Por último, mas não menos importante, se impressione com a diversidade do nosso planeta.